//ler tamanho da 1ª matriz
//ler 1ª matriz
//ler tamanho da 2ª matriz
//ler 2ª matriz
//multiplicar
//retornar resultado


function multMatriz(array1,array2){
    
    var matriz = Array(2).fill().map(() => Array(2).fill(0));
    
    for (var i=0; i<array1.length;i++){
        for (var j=0; j<(array2[0]).length;j++){
            for (var k=0;k<(array1[0]).length;k++){
                matriz[i][j] += (array1[i][k])*(array2[k][j]);
            }
        }
    }
    return(matriz);
}


var array1= [ [4,0], 
            [-1,-1] ];

var array2= [ [-1,3], 
            [2,7] ];

var matriz= multMatriz(array1,array2);

console.log(matriz)

